package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() { //Instance methods

        System.out.println("Enter two integers");
        System.out.print("Lower bound? ");
        int lowerBound = Integer.parseInt (Keyboard.readInput());
        System.out.print("Upper bound? ");
        int upperBound = Integer.parseInt (Keyboard.readInput());

        System.out.print("3 randomly generated numbers: ");

        int range = (upperBound - lowerBound + 1);
        int randNumOne = (int) (lowerBound + (Math.random() * range));
        System.out.print(randNumOne + ", ");

        int randNumTwo = (int) (lowerBound + (Math.random() * range));
        System.out.print(randNumTwo + " and ");

        int randNumThree = (int) (lowerBound + (Math.random() * range));
        System.out.println(randNumThree);

        int minNum = (Math.min (Math.min (randNumOne, randNumTwo), randNumThree));
        System.out.println("Smallest number is " + minNum);


    }
    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
