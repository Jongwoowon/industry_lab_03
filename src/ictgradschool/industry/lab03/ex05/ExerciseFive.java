package ictgradschool.industry.lab03.ex05;

/**
 * Created by anhyd on 3/03/2017.
 */
public class ExerciseFive {

    /**
     * Runs an example program.
     */
    private void start() {
        String letters = "X X O O X O X O X ";

        String row1 = getRow(letters, 1);

        String row2 = getRow(letters, 2);

        String row3 = getRow(letters, 3);

        printRows(row1, row2, row3);

        String leftDiagonal = getLeftDiagonal(row1, row2, row3);

        printDiagonal(leftDiagonal);
    }

    /**
     * TODO Implement this
     */
    public String getRow(String letters, int row) {

        // Extract 6 characters from the String.
        // 1. locating up to 6 characters.
        // 2. /n next line next 6 characters.
        // 3. /n next line next 6 characters.
        // would we want a loop here upto row = 3?

        if (row == 1) {
            String getRow = letters.substring(0, 6);
            return getRow;
        }

        else if (row == 2) {
            String getRow = letters.substring(6, 12);
            return getRow;
        }
        else if (row ==3) {
            String getRow = letters.substring(12, 18);
            return getRow;
        }
        return letters;

        /*String getRow = null;
        for (row = 1; row <= 3; row++) {
            getRow = letters.substring(((row * 6)-6), (6 * row));
        }
        return getRow;*/
    }

    /**
     * TODO Implement this
     */
    public void printRows(String row1, String row2, String row3) {

        System.out.println(row1);
        System.out.println(row2);
        System.out.println(row3);

    }

    /**
     * TODO Implement this
     */
    public String getLeftDiagonal(String row1, String row2, String row3) {

        //1. print out the row number character * 0, 8, 16

        String leftDiagonal = row1.substring(0,2) + row2.substring(2,4) + row3.substring(4,5);
        System.out.print("Diagonal: " + leftDiagonal);//

        return leftDiagonal;
    }

    /**
     * TODO Implement this
     */
    public void printDiagonal(String leftDiagonal) {

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFive ex = new ExerciseFive();
        ex.start();
    }
}
