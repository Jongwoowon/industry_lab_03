package ictgradschool.industry.lab03.ex04;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {

        // TODO Use other methods you create to implement this program's functionality.

        String sentence = getInputFromUser();

        int decimalPlace = getDecimalPlace();

        String truncatedSentence =  newTruncatedSentence(sentence, decimalPlace);

        printNewSentence(decimalPlace, truncatedSentence);

    }

    // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard

    private String getInputFromUser() {

        System.out.print("Please enter an amount: ");
        String sentence = Keyboard.readInput();

        return sentence;
    }
    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard
    private int getDecimalPlace(){

        System.out.print("Please enter the number of decimal places: ");
        int decimalPlace =  Integer.parseInt(Keyboard.readInput());

        return decimalPlace;
    }
    // TODO Write a method which truncates the specified number to the specified number of DP's
    private String newTruncatedSentence(String sentence, int decimalPlace){

        int location = sentence.indexOf('.');//1. find the exact location of the decimal place using indexOf() function
        int newLocation = location + decimalPlace; //2. make newLocation of Decimal cutoff by location + decimal place
        String truncatedSentence = sentence.substring(0, newLocation + 1); //3. substring(0, newlocation)

        return truncatedSentence;
    }
    // TODO Write a method which prints the truncated amount
    private void printNewSentence(int decimalPlace, String truncatedSentence) {

        System.out.print("Amount truncated to " + decimalPlace + " decimalplaces is: " + truncatedSentence);

    }






    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
